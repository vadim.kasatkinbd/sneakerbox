export class HttpClient {
  private readonly baseUrl: string;

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  async get<T>(uri: string, params?: Record<string, any>): Promise<T> {
    const response = await fetch(`${this.baseUrl}${uri}`, params);
    return await response.json();
  }
}
