import { Product } from '@/types/Product';
import { HttpClient } from './Http';
import { getMinMaxPrices } from '@/utils/products';

export class FakestorApi {
  static readonly http = new HttpClient(
    process.env.NEXT_PUBLIC_STORE_API as string
  );

  static async getProducts(sort = 'asc', category?: string) {
    let products: Product[];
    const uri = category
      ? `/products/category/${encodeURI(category)}`
      : '/products';

    products = await this.http.get<Product[]>(uri, { sort });

    return {
      products,
      prices: getMinMaxPrices(products),
    };
  }

  static async getCategories() {
    return this.http.get('/products/categories');
  }

  static async getProduct(id: number) {
    return this.http.get(`/products/${id}`);
  }
}
