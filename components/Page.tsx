import React, { FC } from 'react';
import { Container } from '@/components/Container';
import { Header } from '@/components/Header';

export const Page: FC = ({ children }) => {
  return (
    <>
      <Header />
      <Container>{children}</Container>
    </>
  );
};
