import React, { FC, useCallback, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';

const Slider = styled.div`
  position: relative;
  width: 200px;
`;

const SliderTrack = styled.div`
  border-radius: 3px;
  height: 2px;
  position: absolute;
  background-color: ${(props) => props.theme.colors.range.track};
  width: 100%;
  z-index: 1;
`;

const SliderRange = styled.div`
  border-radius: 3px;
  height: 2px;
  position: absolute;
  background-color: ${(props) => props.theme.colors.range.range};
  z-index: 2;
`;

interface ThumbProps {
  index: string;
}

const Thumb = styled.input<ThumbProps>`
  pointer-events: none;
  position: absolute;
  height: 0;
  width: 200px;
  outline: none;
  -webkit-appearance: none;
  -webkit-tap-highlight-color: transparent;
  z-index: ${(props) => props.index};

  &::-webkit-slider-thumb {
    -webkit-appearance: none;
    -webkit-tap-highlight-color: transparent;
    background-color: ${(props) => props.theme.colors.range.thumb};
    border: none;
    border-radius: 50%;
    cursor: pointer;
    height: 12px;
    width: 12px;
    margin-top: 4px;
    pointer-events: all;
    position: relative;
  }

  &::-moz-range-thumb {
    background-color: ${(props) => props.theme.colors.range.thumb};
    border: none;
    border-radius: 50%;
    cursor: pointer;
    height: 18px;
    width: 18px;
    margin-top: 4px;
    pointer-events: all;
    position: relative;
  }
`;

const SliderValues = styled.div`
  display: flex;
  justify-content: space-between;
`;

const SliderValue = styled.div`
  margin-top: 20px;
  font-weight: 500;
  font-size: 12px;
`;

interface RangeProp {
  min: number;
  max: number;
  onChange: (min: number, max: number) => void;
  gap?: number;
}

export const Range: FC<RangeProp> = ({ min, max, onChange, gap = 1 }) => {
  const [maxVal, setMaxVal] = useState(max);
  const [minVal, setMinVal] = useState(min);

  const track = useRef<HTMLDivElement>(null);
  const minRangeInput = useRef<HTMLInputElement>(null);

  const getPercent = useCallback(
    (value: number) => Math.round(((value - min) / (max - min)) * 100),
    [min, max]
  );

  useEffect(() => {
    if (track.current) {
      track.current.style.left = '0';
      track.current.style.width = '100%';
    }
    setMaxVal(max);
    setMinVal(min);
  }, [max, min]);

  useEffect(() => {
    if (track.current) {
      const minPercent = getPercent(minVal);
      const maxPercent = getPercent(maxVal);

      track.current.style.left = `${minPercent}%`;
      track.current.style.width = `${maxPercent - minPercent}%`;
    }
    if (
      minVal === maxVal &&
      minVal === max &&
      minRangeInput.current &&
      minRangeInput.current.style.zIndex === '3'
    ) {
      minRangeInput.current.style.zIndex = '5';
    } else if (
      minVal !== maxVal &&
      minRangeInput.current &&
      minRangeInput.current.style.zIndex !== '3'
    ) {
      minRangeInput.current.style.zIndex = '3';
    }
    onChange(minVal, maxVal);
  }, [maxVal, minVal]);

  return (
    <>
      <Thumb
        type="range"
        min={min}
        max={max}
        value={minVal}
        index="3"
        ref={minRangeInput}
        onChange={(event) => {
          const value = Math.min(+event.target.value, maxVal - gap);
          setMinVal(Number(value));
        }}
      />
      <Thumb
        type="range"
        min={min}
        max={max}
        value={maxVal}
        index="4"
        onChange={(event) => {
          const value = Math.max(Number(event.target.value), minVal + gap);
          setMaxVal(Number(value));
        }}
      />
      <Slider>
        <SliderTrack />
        <SliderRange ref={track} />
        <SliderValues>
          <SliderValue>{minVal}</SliderValue>
          <SliderValue>{maxVal}</SliderValue>
        </SliderValues>
      </Slider>
    </>
  );
};
