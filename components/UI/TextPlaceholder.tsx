import styled from 'styled-components';

interface TextPlaceholderProps {
  width: number;
  height: number;
}

export const TextPlaceholder = styled.div<TextPlaceholderProps>`
  height: ${(props) => `${props.height}px`};
  width: ${(props) => `${props.width}px`};
  animation: pulse 1s infinite ease-in-out;

  @keyframes pulse {
    0% {
      background-color: ${(props) =>
        props.theme.colors.placeholder.background + '19'};
    }
    50% {
      background-color: ${(props) =>
        props.theme.colors.placeholder.background + '66'};
    }
    100% {
      background-color: ${(props) =>
        props.theme.colors.placeholder.background + '19'};
    }
  }
`;
