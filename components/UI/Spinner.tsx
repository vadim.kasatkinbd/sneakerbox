import styled from 'styled-components';

export const Spinner = styled.div`
  position: relative;
  width: 65px;
  height: 65px;
  border-radius: 50%;
  background: ${(props) => props.theme.colors.spinner.outer};
  animation: animate 1s linear infinite;

  &::before {
    position: absolute;
    content: '';
    background: ${(props) => props.theme.colors.spinner.inner};
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    width: 60%;
    height: 60%;
    border-radius: 50%;
  }

  &::after {
    position: absolute;
    content: '';
    width: 14px;
    height: 14px;
    border-radius: 50%;
    background: ${(props) => props.theme.colors.spinner.start};
    top: 0px;
    right: 26px;
  }
  @keyframes animate {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
`;
