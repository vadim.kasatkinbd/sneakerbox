import styled from 'styled-components';

export const Title = styled.h1`
  margin-bottom: 36px;
  font-weight: 700;
  font-size: 36px;
`;

export const Title2 = styled.h2`
  margin-bottom: 36px;
  font-weight: 700;
  font-size: 24px;
`;
