import React, { FC } from 'react';
import styled from 'styled-components';

const CounterWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const CounterValue = styled.div`
  width: 48px;
  height: 48px;
  margin: 0 19px;
  display: flex;
  align-items: center;
  justify-content: center;
  border: ${(props) => props.theme.border};
  border-radius: 8px;
  font-weight: 500;
  font-size: 16px;
`;

const CounterAction = styled.button`
  font-weight: 500;
  font-size: 20px;
  cursor: pointer;
  border: none;
  background: none;
`;

interface CounterProps {
  val: number;
  onAdd: (val: number) => void;
  onSub: (val: number) => void;
}

export const Counter: FC<CounterProps> = ({ val, onAdd, onSub }) => {
  return (
    <CounterWrapper>
      <CounterAction onClick={() => onSub(val - 1)}>−</CounterAction>
      <CounterValue>{val}</CounterValue>
      <CounterAction onClick={() => onAdd(val + 1)}>+</CounterAction>
    </CounterWrapper>
  );
};
