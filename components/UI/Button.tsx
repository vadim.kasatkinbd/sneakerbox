import styled from 'styled-components';

export const Button = styled.button`
  background: ${(props) => props.theme.colors.button.primary};
  font-weight: 700;
  font-size: 16px;
  color: ${(props) => props.theme.colors.button.color};
  padding: 10px 24px;
  border: none;
  cursor: pointer;
  &:hover {
    background-color: ${(props) => props.theme.colors.button.primary + 'cc'};
  }
`;
