import styled from 'styled-components';

interface TextProps {
  weight?: number | string;
  size?: string;
  light?: boolean;
}

export const Text = styled.p<TextProps>`
  font-weight: ${(props) => props.weight ?? 500};
  font-size: ${(props) => props.size ?? '16px'};
  line-height: 24px;
  color: ${(props) =>
    props.light ? props.theme.colors.text.light : props.theme.colors.text.main};
`;
