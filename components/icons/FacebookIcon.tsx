import React from 'react';
import { PathSocial, SvgSocial, RectSocial } from './Svg';

export const FacebookIcon = () => {
  return (
    <SvgSocial viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
      <RectSocial rx="8" />
      <PathSocial d="M20 9.33337H18C17.1159 9.33337 16.2681 9.68456 15.6429 10.3097C15.0178 10.9348 14.6666 11.7827 14.6666 12.6667V14.6667H12.6666V17.3334H14.6666V22.6667H17.3333V17.3334H19.3333L20 14.6667H17.3333V12.6667C17.3333 12.4899 17.4035 12.3203 17.5286 12.1953C17.6536 12.0703 17.8231 12 18 12H20V9.33337Z" />
    </SvgSocial>
  );
};
