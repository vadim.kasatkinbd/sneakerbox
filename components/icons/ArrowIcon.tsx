import React from 'react';
import { Path, Svg } from './Svg';

export const ArrowIcon = () => {
  return (
    <Svg
      viewBox="0 0 10 6"
      xmlns="http://www.w3.org/2000/svg"
      width={'10px'}
      height={'6px'}
    >
      <Path d="M9 5L5 1L1 5" />
    </Svg>
  );
};
