import styled from 'styled-components';

interface SvgProps {
  width: string;
  height: string;
}

export const Svg = styled.svg<SvgProps>`
  width: ${(props) => props.width};
  height: ${(props) => props.width};
  fill: none;
`;

export const SvgSocial = styled.svg`
  width: 32px;
  height: 32px;
  fill: none;
`;

export const Path = styled.path`
  stroke: ${(props) => props.theme.colors.icons.main};
  stroke-width: 2;
  stroke-linecap: round;
  stroke-linejoin: round;
`;

export const PathSocial = styled(Path)`
  stroke: ${(props) => props.theme.colors.icons.social};
  stroke-width: 1;
`;

export const RectSocial = styled.rect`
  width: 32px;
  height: 32px;
  fill: ${(props) => props.theme.colors.icons.main};
  fill-opacity: 0.04;
`;
