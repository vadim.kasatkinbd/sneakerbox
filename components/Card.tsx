import React, { FC, ReactNode, useState } from 'react';
import styled from 'styled-components';
import { Text } from '@/components/UI/Text';
import { ArrowIcon } from '@/components/icons/ArrowIcon';

const Wrapper = styled.div`
  padding: 32px;
  border-bottom: ${(props) => props.theme.border};
  &:last-child {
    border-bottom: none;
  }
`;

const WrapperCollapse = styled(Wrapper)`
  padding: 0 0 32px;
`;

const TitleWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 16px;
`;

interface ImageProps {
  collapse: boolean;
}

const ImageWrapper = styled.div<ImageProps>`
  display: flex;
  align-items: center;
  cursor: pointer;
  transform: rotate(${(props) => (props.collapse ? '180deg' : '0deg')});
  transition: transform 0.4s;
`;

interface BodyProps {
  collapse: boolean;
}

const Body = styled.div<BodyProps>`
  overflow: hidden;
  height: auto;
  max-height: ${(props) => (props.collapse ? '0' : '500px')};
  transition: all 0.4s;
`;

interface CardProps {
  title: string;
  children: ReactNode;
  noPadding?: boolean;
  collapsed?: boolean;
}

export const Card: FC<CardProps> = ({ title, children }) => {
  return (
    <Wrapper>
      <TitleWrapper>
        <Text>{title}</Text>
      </TitleWrapper>
      <Body collapse={false}>{children}</Body>
    </Wrapper>
  );
};

export const CardCollapsed: FC<CardProps> = ({ title, children }) => {
  const [collapse, setCollapse] = useState(false);

  return (
    <WrapperCollapse>
      <TitleWrapper onClick={() => setCollapse((state) => !state)}>
        <Text>{title}</Text>
        <ImageWrapper collapse={collapse}>
          <ArrowIcon />
        </ImageWrapper>
      </TitleWrapper>

      <Body collapse={collapse}>{children}</Body>
    </WrapperCollapse>
  );
};
