import React, { ReactNode, FC } from 'react';
import { createPortal } from 'react-dom';
import styled from 'styled-components';
import { Container } from '@/components/Container';

export const ModalWrapper = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  z-index: 1000;
  background: ${(props) => props.theme.colors.modal};
`;

interface ModalProps {
  onClick?: () => void;
  children: ReactNode;
}

export const Modal: FC<ModalProps> = ({ onClick, children }) => {
  return createPortal(
    <ModalWrapper onClick={() => onClick && onClick()}>
      <Container onClick={(e) => e.stopPropagation()}>{children}</Container>
    </ModalWrapper>,
    document.querySelector('#modal') as HTMLDivElement
  );
};
