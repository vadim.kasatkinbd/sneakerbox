import React, { useEffect } from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useAppDispatch, useAppSelector } from '@/features/hooks';
import { fetchCategories } from '@/features/categoriesSlice';
import { TextPlaceholder } from '@/components/UI/TextPlaceholder';

const List = styled.ul`
  list-style: none;
  display: flex;
`;

type ListItemProps = {
  active?: boolean;
};

const ListItem = styled.li<ListItemProps>`
  margin-right: 32px;
  padding-bottom: 4px;
  border-bottom: ${(props) => (props.active ? '2px solid black' : '')};
  &::last-child {
    margin-right: 0;
  }
`;

const ListLink = styled.a`
  font-size: 14px;
  font-weight: 500;
  text-transform: capitalize;
  text-decoration: none;
  cursor: pointer;
  color: ${(props) => props.theme.colors.link.main};
  &:hover {
    color: ${(props) => props.theme.colors.link.hover};
  }
`;

export const CategoriesList = () => {
  const { categories, status } = useAppSelector((state) => state.categories);

  const router = useRouter();
  const { category: curCategory } = router.query;

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchCategories());
  }, []);

  if (status === 'error') {
    router.push('500');
  }

  return (
    <List>
      {status === 'loading'
        ? Array(4)
            .fill(<TextPlaceholder width={100} height={15} />)
            .map((item, index) => <ListItem key={index}>{item}</ListItem>)
        : categories.map((category) => (
            <ListItem key={category} active={curCategory === category}>
              <Link href={{ pathname: '/', query: { category } }} passHref>
                <ListLink>{category}</ListLink>
              </Link>
            </ListItem>
          ))}
    </List>
  );
};
