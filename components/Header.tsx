import React, { FC } from 'react';
import Image from 'next/image';
import styled from 'styled-components';
import { Container } from '@/components/Container';
import { CategoriesList } from '@/components/CategoriesList';
import Link from 'next/link';
import { CartIcon } from '@/components/icons/CartIcon';
import { HeartIcon } from '@/components/icons/HeartIcon';
import { UserIcon } from '@/components/icons/UserIcon';

const HeaderWrapper = styled.header`
  border-bottom: ${(props) => props.theme.border};
  grid-area: head;
`;

const HeaderContent = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 64px;
  height: ${(props) => props.theme.headerHeight};
`;

const HeaderMenu = styled.ul`
  list-style: none;
  display: flex;
`;

const HeaderMenuItem = styled.li`
  margin-right: 33px;
  &::last-child {
    margin-right: 0;
  }
`;

export const Header: FC = () => {
  const menuItems = [
    { url: 'cart', component: <CartIcon /> },
    { url: 'favorites', component: <HeartIcon /> },
    { url: 'user', component: <UserIcon /> },
  ];

  return (
    <HeaderWrapper>
      <Container>
        <HeaderContent>
          <Link href={'/'}>
            <a>
              <Image src="/logo.png" alt="logo" width={99} height={36} />
            </a>
          </Link>

          <CategoriesList />
          <HeaderMenu>
            {menuItems.map(({ url, component }) => (
              <HeaderMenuItem key={url}>
                <Link href={`/${url}`}>
                  <a>{component}</a>
                </Link>
              </HeaderMenuItem>
            ))}
          </HeaderMenu>
        </HeaderContent>
      </Container>
    </HeaderWrapper>
  );
};
