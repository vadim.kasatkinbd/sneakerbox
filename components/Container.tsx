import styled from 'styled-components';

export const Container = styled.div`
  max-width: 1440px;
  margin: 0 auto;
  width: 100%;
  flex-grow: 1;
`;
