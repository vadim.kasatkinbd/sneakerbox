import React, { useState } from 'react';
import { createPortal } from 'react-dom';
import styled from 'styled-components';
import Image from 'next/image';
import { CardCollapsed } from '@/components/Card';
import { Counter } from '@/components/UI/Counter';
import { Button } from '@/components/UI/Button';
import { useAppDispatch, useAppSelector } from '@/features/hooks';
import { setProductCurrent } from '@/features/productsSlice';
import { addItem } from '@/features/cartSlice';
import { Product } from '@/types/Product';
import { HeartIcon } from '@/components/icons/HeartIcon';
import { Text } from '@/components/UI/Text';
import { Title } from './UI/Title';
import { Modal } from './Modal';

const ContentWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const View = styled.div`
  width: 944px;
  height: 820px;
  padding: 64px 96px;
  background: ${(props) => props.theme.colors.productModal.view};
`;

const ViewHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  margin-bottom: 15px;
`;

const ViewBody = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const TitleWrapper = styled.div``;

const FavoriteWrapper = styled.div`
  cursor: pointer;
  width: 25px;
  height: 25px;
  flex-shrink: 0;
  margin-left: 7px;
  margin-top: 7px;
`;

const Description = styled.div`
  width: 496px;
  height: 820px;
  background: ${(props) => props.theme.colors.productModal.description};
  padding: 32px 64px;
`;

const CounterWrapper = styled.div`
  margin-top: 32px;
  display: flex;
`;

const ProductCounter = styled.div`
  margin-right: 30px;
`;

export const ProductModal = () => {
  const [count, setCount] = useState(1);

  const product = useAppSelector(
    (state) => state.products.productCurrent as Product
  );
  const dispatch = useAppDispatch();

  const onAdd = (val: number) => setCount(val);
  const onSub = (val: number) => setCount(val < 1 ? 1 : val);
  return (
    <Modal
      onClick={() => {
        dispatch(setProductCurrent(null));
      }}
    >
      <ContentWrapper>
        <View>
          <ViewHeader>
            <TitleWrapper>
              <Title>{product.title}</Title>
              <Text size="18px" light>
                {product.price} $
              </Text>
            </TitleWrapper>
            <FavoriteWrapper>
              <HeartIcon />
            </FavoriteWrapper>
          </ViewHeader>
          <ViewBody>
            <Image
              src={product.image}
              alt={product.title}
              width={500}
              height={500}
              objectFit="contain"
            />
          </ViewBody>
        </View>
        <Description>
          <CardCollapsed title="Description">
            <Text light weight={400}>
              {product.description}
            </Text>
          </CardCollapsed>
          <CounterWrapper>
            <ProductCounter>
              <Counter val={count} onAdd={onAdd} onSub={onSub} />
            </ProductCounter>
            <Button onClick={() => dispatch(addItem({ product, count }))}>
              Add to card
            </Button>
          </CounterWrapper>
        </Description>
      </ContentWrapper>
    </Modal>
  );
};
