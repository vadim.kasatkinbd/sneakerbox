import React from 'react';
import styled from 'styled-components';
import { Container } from '@/components/Container';
import { FacebookIcon } from '@/components/icons/FacebookIcon';
import { InstagramIcon } from '@/components/icons/InstagramIcon';
import { TwitterIcon } from '@/components/icons/TwitterIcon';
import { Text } from '@/components/UI/Text';

const FooterWrapper = styled.footer`
  margin-top: auto;
  padding-top: 32px;
  padding-bottom: 64px;
  border-top: ${(props) => props.theme.border};
`;

const SocialsWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 4px;
`;

const Socials = styled.div`
  display: flex;
  align-items: center;
`;

const SocialImageLink = styled.a`
  margin-left: 12px;
`;

const Contacts = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Footer = () => {
  return (
    <FooterWrapper>
      <Container>
        <SocialsWrapper>
          <Text light>Don’t missout on once-in-a-while-deals:</Text>
          <Socials>
            <SocialImageLink href="#">
              <TwitterIcon />
            </SocialImageLink>
            <SocialImageLink href="#">
              <InstagramIcon />
            </SocialImageLink>
            <SocialImageLink href="#">
              <FacebookIcon />
            </SocialImageLink>
          </Socials>
        </SocialsWrapper>

        <Contacts>
          <Text light>Support line: +250 788 467 808</Text>
          <Text light>Copyright 2021 © Sneaker City ltd</Text>
        </Contacts>
      </Container>
    </FooterWrapper>
  );
};
