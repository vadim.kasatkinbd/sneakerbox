import React from 'react';
import styled from 'styled-components';
import { Footer } from '@/components/Footer';

const Wrapper = styled.main`
  display: flex;
  flex-direction: column;
  height: calc(100vh - ${(props) => props.theme.headerHeight});
  width: 100%;
  padding: 32px 0;
  flex-grow: 1;
`;

const Content = styled.div`
  flex-grow: 1;
`;

export const Main: React.FC = ({ children }) => {
  return (
    <Wrapper>
      <Content>{children}</Content>
      <Footer />
    </Wrapper>
  );
};
