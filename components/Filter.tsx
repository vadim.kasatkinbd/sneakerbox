import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useAppDispatch, useAppSelector } from '@/features/hooks';
import { filter, setSort } from '@/features/productsSlice';
import { Card } from '@/components/Card';
import { Range } from '@/components/UI/Range';

const FilterWrapper = styled.div`
  width: 100%;
`;

const FilterRadioLabel = styled.label`
  display: flex;
  align-items: center;
  margin-bottom: 14px;
  font-size: 14px;
  color: ${(props) => props.theme.colors.text.light};
  cursor: pointer;
  &:last-of-type {
    margin-bottom: 0;
  }
`;

const FilterRidioWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const FilterRadio = styled.input`
  margin-right: 12px;
`;

export const Filter = () => {
  const {
    sort,
    maxPrice = 1000,
    minPrice = 0,
  } = useAppSelector((state) => state.products);
  const dispatch = useAppDispatch();

  const [filterPrice, setFilterPrice] = useState({
    min: minPrice,
    max: maxPrice,
  });
  const [filterRating, setFilterRating] = useState({ min: 0, max: 5 });

  const changePriceRange = (min: number, max: number) => {
    setFilterPrice({ min, max });
  };

  const changeRatingRange = (min: number, max: number) => {
    setFilterRating({ min, max });
  };

  useEffect(() => {
    dispatch(
      filter({
        price: filterPrice,
        rating: filterRating,
      })
    );
  }, [filterPrice, filterRating]);

  return (
    <FilterWrapper>
      <Card title="Sort">
        <FilterRidioWrapper>
          <FilterRadioLabel>
            <FilterRadio
              type={'radio'}
              onChange={() => dispatch(setSort('asc'))}
              checked={sort === 'asc'}
            />
            ASC
          </FilterRadioLabel>
          <FilterRadioLabel>
            <FilterRadio
              type={'radio'}
              onChange={() => dispatch(setSort('desc'))}
              checked={sort === 'desc'}
            />
            DESC
          </FilterRadioLabel>
        </FilterRidioWrapper>
      </Card>
      <Card title="Price range">
        <Range min={minPrice} max={maxPrice} onChange={changePriceRange} />
      </Card>
      <Card title="Rating range">
        <Range min={0} max={5} gap={0} onChange={changeRatingRange} />
      </Card>
    </FilterWrapper>
  );
};
