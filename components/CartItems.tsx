import React from 'react';
import Image from 'next/image';
import styled from 'styled-components';
import { addItem, deleteItem } from '@/features/cartSlice';
import { useAppDispatch, useAppSelector } from '@/features/hooks';
import { Counter } from '@/components/UI/Counter';
import { Text } from '@/components/UI/Text';

const Wrapper = styled.div`
  margin-right: 71px;
`;
const Item = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
  padding: 24px 32px;
  align-items: center;
  background: ${(props) => props.theme.colors.cartItems.item};
`;
const ImageWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${(props) => props.theme.colors.cartItems.imageWrapper};
  border-radius: 8px;
  width: 96px;
  height: 80px;
  margin-right: 24px;
`;

const Sum = styled.div`
  width: 100px;
  text-align: right;
  font-weight: 500;
  font-size: 18px;
`;

const Preview = styled.div`
  display: flex;
  align-items: center;
  width: 320px;
`;
const Desc = styled.div``;
const Price = styled(Text)`
  width: 100px;
`;
const Name = styled(Text)`
  margin-bottom: 5px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  width: 195px;
`;

export const CartItems = () => {
  const { items } = useAppSelector((store) => store.cart);
  const dispatch = useAppDispatch();

  return (
    <Wrapper>
      {items?.map(({ product, count }) => (
        <Item key={product.id}>
          <Preview>
            <ImageWrapper>
              <Image
                src={product.image}
                width={72}
                height={64}
                objectFit={'contain'}
              />
            </ImageWrapper>
            <Desc>
              <Name size="18px">{product.title}</Name>
              <Price light>{product.price} $</Price>
            </Desc>
          </Preview>
          <Counter
            val={count}
            onAdd={() => dispatch(addItem({ product, count: 1 }))}
            onSub={(val) => {
              if (val < 1) {
                dispatch(deleteItem(product.id));
              } else {
                dispatch(addItem({ product, count: -1 }));
              }
            }}
          />
          <Sum>{(product.price * count).toFixed(2)} $</Sum>
        </Item>
      ))}
    </Wrapper>
  );
};
