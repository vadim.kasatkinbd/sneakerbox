import React from 'react';
import { Spinner } from '@/components/UI/Spinner';
import { Modal } from './Modal';
import styled from 'styled-components';

const SpinnerWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const PageLoader = () => {
  return (
    <Modal>
      <SpinnerWrapper>
        <Spinner />
      </SpinnerWrapper>
    </Modal>
  );
};
