import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import styled from 'styled-components';
import Image from 'next/image';
import { fetchProducts, setProductCurrent } from '@/features/productsSlice';
import { useAppSelector, useAppDispatch } from '@/features/hooks';
import { Text } from '@/components/UI/Text';
import { PageLoader } from '@/components/PageLoader';
import { ProductModal } from '@/components/ProductModal';

const ProductListWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -12px;
`;

const Product = styled.div`
  width: 310px;
  margin: 0 12px 64px;
`;

const ProductName = styled(Text)`
  margin-bottom: 7px;
  cursor: pointer;
`;

const ImageWrapper = styled.div`
  margin-bottom: 24px;
`;

const ErrorWrapper = styled.div`
  font-size: 26px;
`;

export const ProductList = () => {
  const dispatch = useAppDispatch();
  const { productsFilterd, status, sort, productCurrent } = useAppSelector(
    (state) => state.products
  );

  const router = useRouter();
  const { category } = router.query;

  useEffect(() => {
    dispatch(
      fetchProducts({ sort, category: !category ? category : String(category) })
    );
  }, [sort, category]);

  if (status === 'error') {
    return <ErrorWrapper>Oops</ErrorWrapper>;
  }

  return (
    <ProductListWrapper>
      {status === 'loading' && <PageLoader />}
      {productsFilterd.map((product) => (
        <Product key={product.id}>
          <ImageWrapper>
            <Image
              src={product.image}
              width={308}
              height={310}
              objectFit={'contain'}
            />
          </ImageWrapper>
          <ProductName
            size="18px"
            onClick={() => dispatch(setProductCurrent(product))}
          >
            {product.title}
          </ProductName>
          <Text light>{product.price} $</Text>
        </Product>
      ))}
      {productCurrent && <ProductModal />}
    </ProductListWrapper>
  );
};
