import { FakestorApi } from '@/api/Fakestore';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

interface CategoriesState {
  categories: string[];
  status: 'loading' | 'error' | 'success' | null;
}

const initialState: CategoriesState = {
  categories: [],
  status: null,
};

export const fetchCategories = createAsyncThunk(
  'categories/fetchCategories',
  async () => {
    return await FakestorApi.getCategories();
  }
);

const categoriesSlice = createSlice({
  name: 'categories',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchCategories.pending, (state, action) => {
      state.status = 'loading';
    });
    builder.addCase(fetchCategories.fulfilled, (state, action) => {
      state.status = 'success';
      state.categories = action.payload as string[];
    });
    builder.addCase(fetchCategories.rejected, (state) => {
      state.status = 'error';
    });
  },
});

export default categoriesSlice.reducer;
