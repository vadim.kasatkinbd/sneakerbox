import { FakestorApi } from '@/api/Fakestore';
import { Product } from '@/types/Product';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

export type Sort = 'asc' | 'desc';

interface ProductsState {
  products: Product[];
  productsFilterd: Product[];
  status: 'loading' | 'error' | 'success' | null;
  sort: Sort;
  minPrice?: number;
  maxPrice?: number;
  productCurrent: Product | null;
}

const initialState: ProductsState = {
  products: [],
  productsFilterd: [],
  status: null,
  sort: 'asc',
  productCurrent: null,
};

export const fetchProducts = createAsyncThunk(
  'products/fetchProducts',
  async ({ sort, category }: { sort?: Sort; category?: string }) => {
    return await FakestorApi.getProducts(sort, category);
  }
);

const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    setSort(state, action) {
      state.sort = action.payload;
    },
    filter(state, action) {
      const products = state.products.filter(
        ({ price, rating }) =>
          price >= action.payload.price.min &&
          price <= action.payload.price.max &&
          rating.rate >= action.payload.rating.min &&
          rating.rate <= action.payload.rating.max
      );
      state.productsFilterd = products;
    },
    setProductCurrent(state, action) {
      state.productCurrent = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchProducts.pending, (state) => {
      state.status = 'loading';
    });
    builder.addCase(fetchProducts.fulfilled, (state, action) => {
      state.status = 'success';
      state.products = action.payload.products;
      state.productsFilterd = action.payload.products;
      state.minPrice = action.payload.prices.min;
      state.maxPrice = action.payload.prices.max;
    });
    builder.addCase(fetchProducts.rejected, (state) => {
      state.status = 'error';
    });
  },
});

export const { setSort, filter, setProductCurrent } = productsSlice.actions;
export default productsSlice.reducer;
