import ProductsReducer from './productsSlice';
import categoriesReducer from './categoriesSlice';
import cartReducer from './cartSlice';

import { combineReducers, configureStore } from '@reduxjs/toolkit';

const rootReducer = combineReducers({
  products: ProductsReducer,
  categories: categoriesReducer,
  cart: cartReducer,
});

const makeStore = () => {
  return configureStore({
    reducer: rootReducer,
  });
};

const store = makeStore();

export type AppState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

export default store;
