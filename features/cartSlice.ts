import { Product } from '@/types/Product';
import { createSlice } from '@reduxjs/toolkit';

interface CartItem {
  product: Product;
  count: number;
}

interface CartState {
  items: CartItem[];
  totalPrice: number;
}

const initialState: CartState = {
  items: [],
  totalPrice: 0,
};

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addItem(state, action) {
      const productId = state.items.findIndex(
        ({ product }) => product.id === action.payload.product.id
      );

      if (productId === -1) {
        state.items.push(action.payload);
      } else {
        state.items[productId].count += action.payload.count;
      }

      const sum = state.items.reduce(
        (total, { product, count }) => total + product.price * count,
        0
      );

      state.totalPrice = parseFloat(sum.toFixed(2));
    },
    deleteItem(state, action) {
      state.items = state.items.filter(
        (item) => item.product.id !== action.payload
      );

      const sum = state.items.reduce(
        (total, { product, count }) => total + product.price * count,
        0
      );

      state.totalPrice = parseFloat(sum.toFixed(2));
    },
  },
});

export const { addItem, deleteItem } = cartSlice.actions;
export default cartSlice.reducer;
