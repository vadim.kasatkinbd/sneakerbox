import type { NextPage } from 'next';
import styled from 'styled-components';
import { Filter } from '@/components/Filter';
import { Main } from '@/components/Main';
import { Page } from '@/components/Page';
import { ProductList } from '@/components/ProductList';

const Aside = styled.aside`
  padding: 0 32px;
`;

const ContentWrapper = styled.div`
  display: flex;
`;

const Home: NextPage = () => {
  return (
    <Page>
      <ContentWrapper>
        <Aside>
          <Filter />
        </Aside>
        <Main>
          <ProductList />
        </Main>
      </ContentWrapper>
    </Page>
  );
};

export default Home;
