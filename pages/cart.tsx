import { NextPage } from 'next/types';
import styled from 'styled-components';
import { Button } from '@/components/UI/Button';
import { useAppSelector } from '@/features/hooks';
import { CartItems } from '@/components/CartItems';
import { Title } from '@/components/UI/Title';
import { Text } from '@/components/UI/Text';
import { Main } from '@/components/Main';
import { Page } from '@/components/Page';

const Content = styled.div`
  display: flex;
  padding: 32px 64px 0;
`;

const CartOrder = styled.div`
  width: 368px;
`;
const CartOrderBody = styled.div`
  padding-bottom: 10px;
  border-bottom: ${(props) => props.theme.border};
  margin-bottom: 24px;
`;
const CartOrderRow = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 12px;
`;

const CartOrderTotal = styled.div`
  margin-bottom: 64px;
  font-weight: 500;
  font-size: 18px;
  text-align: right;
`;

const CartWrapper = styled.div`
  width: 100%;
`;

const CartOrderTitle = styled(Text)`
  margin-bottom: 32px;
`;

const Cart: NextPage = () => {
  const { totalPrice } = useAppSelector((store) => store.cart);

  return (
    <Page>
      <Main>
        <Content>
          <CartWrapper>
            <Title>Your shopping cart</Title>
            <CartItems />
          </CartWrapper>
          <CartOrder>
            <CartOrderTitle light size="18px">
              Order summary
            </CartOrderTitle>
            <CartOrderBody>
              <CartOrderRow>
                <Text>Sub total</Text>
                <Text>{totalPrice} $</Text>
              </CartOrderRow>
              <CartOrderRow>
                <Text>Delivery fee</Text>
                <Text>0 $</Text>
              </CartOrderRow>
            </CartOrderBody>
            <CartOrderTotal>{totalPrice} $</CartOrderTotal>
            <Button>Proceed to checkout</Button>
          </CartOrder>
        </Content>
      </Main>
    </Page>
  );
};

export default Cart;
