import { Product } from './../types/Product';
export const getMinMaxPrices = (products: Product[]) => {
  const prices = products.map((product) => product.price);
  return {
    min: Math.trunc(Math.min(...prices)),
    max: Math.ceil(Math.max(...prices)),
  };
};
