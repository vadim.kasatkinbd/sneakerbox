export const theme = {
  colors: {
    text: {
      main: '#000',
      hover: '#7f7f7f',
      light: 'rgba(0, 0, 0, 0.65)',
    },
    button: {
      primary: '#d90429',
      color: '#fff',
    },
    placeholder: {
      background: '#A5A5A5',
    },
    range: {
      track: '#ced4da',
      range: '#000',
      thumb: '#000',
    },
    spinner: {
      outer: 'conic-gradient(#0000, #000)',
      inner: 'rgba(196, 196, 196)',
      start: '#000',
    },
    link: {
      main: '#000',
      hover: '#7f7f7f',
    },
    modal: 'rgba(196, 196, 196, 0.3)',
    productModal: {
      view: '#f5f5f5',
      description: '#fff',
    },
    cartItems: {
      item: 'rgba(0, 0, 0, 0.04)',
      imageWrapper: '#fff',
    },
    icons: {
      main: '#000',
      social: '#EF233C',
    },
  },
  headerHeight: '81px',
  border: '1px solid rgba(0, 0, 0, 0.15)',
};
